Rysino
======

Rysino is "Arduino like" board for Rysy Core based on Max10 FPGA.

Now available on [AVT](https://sklep.avt.pl/avt5726.html?fbclid=IwAR1AYIGiUBc2YnRXeU4rKx86XCYT-YRUQqFZF2YvC7An3WFgjYAVuyJ_DaE).

![pinout](pinout.png)