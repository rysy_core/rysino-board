/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2019 Rafal Kozik
 * All rights reserved.
 */

`default_nettype none

module board_test (
	input wire clk,
	input wire rst,
	input wire rx,
	input wire [15:2]digital,
	input wire [5:0]analog,
	input wire [3:0]switch,
	output logic tx,
	output logic [7:0]led
);

	logic [25:0]counter;
	
	// Leds ans switches
	always_ff @(posedge clk)
		if (~rst) begin
			counter <= '0;
			led <= '0;
		end else begin
			counter <= counter + 26'd1;
			led <= {~switch, counter[25-:4]};
		end
	
	// Uart echo
	always_ff @(posedge clk)
		tx <= rx;

endmodule

`default_nettype wire